<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BrendasMeatandThree
 */

?>
<figure class="hero">
	<?php
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			the_post_thumbnail();
		}else{
			echo "<img src=\"http://placehold.it/1040x500\">";
		}
	?>
</figure>
