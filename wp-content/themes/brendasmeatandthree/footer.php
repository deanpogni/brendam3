<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BrendasMeatandThree
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script type='text/javascript' src='https://code.jquery.com/jquery-1.12.0.min.js'></script>
<script>
$(".menu-course-navigation-menu .page_item a:contains('Weekend')").html(function(_, html) {
   return  html.replace(/(Weekend)/g, '<span>$1</span>')
});

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56581021-2', 'auto');
  ga('send', 'pageview');

</script>


<!-- Seat Ninja -->
<script src="https://sdk.seatninja.com/web/sn.js"></script>
<script src="https://sdk.seatninja.com/web/sn-ui.js"></script>

<script>
	function callback(error, result)
        {
            if (error)
            {
                //handle error
                console.log(error);
            }
            else
            {
                //do something with result object
                console.log(result);
            }
        }
        sn.init({ apiKey: "wZO3jh8lDI3qEKW4A7dLc9Z51kCoPB8A71Ideno5", restaurantId: 55419, callback: callback, isDebug: false });
        
        var reservationElement = document.querySelector('#sn-reservations');
        if (reservationElement !== null) {
	        sn.waitListUi("sn-reservations");	        
        }
        
		var waitlistElement = document.querySelector('#sn-waitlist');
        if (waitlistElement !== null) {
	        sn.reservationUi("sn-waitlist");
        }
        
        var comeByLink = document.querySelector("#menu-item-2304 a");
        comeByLink.addEventListener('click', function(event) {
	        event.preventDefault();
        });
</script>
<!-- Seat Ninja -->
</body>
</html>
