<?php
/**
 * Template Name: Home
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BrendasMeatandThree
 */

get_header(); ?>

	<div id="primary" class="content-area home">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<figure class="hero home-hero">
					<?php the_post_thumbnail(); ?>
					<?php get_template_part( 'template-parts/content', 'page' ); ?>
				</figure>


			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->

			<div class="three-col">
						<?php the_field('secondary_text'); ?>
			</div>

			<div class="three-col">
				<?php the_field('home_news_and_events'); ?>
			</div>

			<div class="three-col">
						<?php the_field('home_contact'); ?>
			</div>

	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
