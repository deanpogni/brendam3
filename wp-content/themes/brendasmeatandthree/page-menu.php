<?php
/**
 * Template Name: Menu
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BrendasMeatandThree
 */

get_header(); ?>

	<div id="primary" class="content-area menu-page">

		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'hero' ); ?>

<!-- 				<?php wp_nav_menu( array( 'theme_location' => 'course_side' ) ); ?> -->

				    <?php /* Creates a menu for pages beneath the level of the current page */
            if (is_page() and ($notfound != '1')) {
            $current_page = $post->ID;
            while($current_page) {
            $page_query = $wpdb->get_row("SELECT ID, post_title, post_status, post_parent FROM $wpdb->posts WHERE ID = '$current_page'");
            $current_page = $page_query->post_parent;
            }
            $parent_id = $page_query->ID;
            $parent_title = $page_query->post_title;

            if ($wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_parent = '$parent_id' AND post_status != 'attachment'")) { ?>
            <div class="menu-course-navigation-menu-container">
              <ul class="menu-course-navigation-menu">
              <?php wp_list_pages('sort_column=menu_order&title_li=&child_of='. $parent_id); ?>
              </ul>
            </div>

          </div>
            <?php } } ?>

				<div class="menu-content">


					<?php get_template_part( 'template-parts/content', 'page' ); ?>

					<?php if (is_page( 39 ) ): ?>
						<div class="meat-and-three">
								<?php the_field('meat_and_three'); ?>
								<div class="menu_left_column menu-column">
									<?php the_field('supper_left_column_meat'); ?>
								</div>
								<div class="menu_right_column menu-column">
									<?php the_field('supper_right_column_sides'); ?>
								</div>
						</div>
						<h2>A la carte <span>(Available nightly)</span></h2>
					<?php endif ?>

					<div class="menu_left_column menu-column">
							<?php the_field('menu_left_column'); ?>
					</div>
					<div class="menu_right_column menu-column">
							<?php the_field('menu_right_column'); ?>
					</div>


					<p class="surcharge-copy">
						A 5% surcharge will be added to all purchases to comply with SF Employer Mandates.
					</p>

				</div>



			<?php endwhile; // End of the loop. ?>


		</main><!-- #main -->



	</div><!-- #primary -->



<?php get_footer(); ?>
